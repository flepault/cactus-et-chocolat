export const environment = {
    production: true,
    firebaseAuthURL:
        'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyC1d9WHE2Bf_Axh3krbBCsUd43XnXDKQDA',
    firebaseHttpURL: 'https://us-central1-cactus-et-chocolat.cloudfunctions.net',
    firebaseUploadImageURL: 'https://us-central1-cactus-et-chocolat.cloudfunctions.net/uploadImageRecette'
};
