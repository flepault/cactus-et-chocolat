import { Component, OnInit } from '@angular/core';
import { environment } from '../environments/environment';
import { LoadGallery } from './state/firebase/firebase.action';
import { Store } from '@ngxs/store';
import { Router } from '@angular/router';
import { ResponsiveService } from './shared/service/responsive/responsive.service';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-cactus-et-chocolat',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    public isProd: boolean;

    menu = false;

    isMobile$: Observable<boolean>;

    constructor(private store: Store,
                private router: Router,
                public responsiveService: ResponsiveService) {
        this.isProd = environment.production;
        this.isMobile$ = responsiveService.isMobile();
    }

    ngOnInit(): void {
        this.loadGallery();
        this.router.events.subscribe(event => {
            this.closeMenu();
        });
    }

    private loadGallery(): void {
        this.store.dispatch(new LoadGallery());
    }

    openMenu(): void {
        this.menu = true;
    }

    closeMenu(): void {
        this.menu = false;
    }

}
