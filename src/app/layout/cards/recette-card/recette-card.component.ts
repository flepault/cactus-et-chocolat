import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ReadRecetteModel, Recette } from '../../../shared/model/recette.model';
import { AuthentificationService } from '../../../shared/service/authentification/authentification.service';
import { EditerRecetteModalComponent } from '../../admin/editer-recette-modal/editer-recette-modal.component';
import { RecetteService } from '../../../shared/service/recette/recette.service';
import { first } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-recette-card',
    templateUrl: './recette-card.component.html',
    styleUrls: ['./recette-card.component.css']
})
export class RecetteCardComponent implements OnInit {

    @Input()
    public recette: Recette;

    @Input()
    public full = true;

    private id: string;

    constructor(
        public dialog: MatDialog,
        public authentificationService: AuthentificationService,
        public recetteService: RecetteService,
        public route: ActivatedRoute,
        public router: Router
    ) {
    }

    ngOnInit(): void {
        if (!!this.recette) {
            this.initImage();
        } else {
            this.getRecette();
        }
    }

    private getRecette(): void {
        this.id = this.route.snapshot.paramMap.get('id');
        this.recetteService.getRecette(this.id)
            .pipe(first())
            .subscribe((readRecetteModel: ReadRecetteModel) => {
                this.recette = readRecetteModel.recette;
                this.initImage();
            });
    }

    private initImage(): void {
        const elem = document.createElement('div');
        if (elem) {
            elem.innerHTML = this.recette.texte;
            const images = elem.getElementsByTagName('img');

            if (images && images[0]) {
                this.recette.image = images[0].src;
            }
        }
    }

    public redirectRecette(): void {
        this.router.navigate(['article/' + this.recette.id]);
    }

    public openDialogEdit(): void {
        this.dialog.closeAll();
        const dialogRef = this.dialog.open(EditerRecetteModalComponent, {
            data: {recette: this.recette},
            disableClose: true
        });

        dialogRef.afterClosed().subscribe(result => {
        });
    }
}
