import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';

import { RecetteCardComponent } from './recette-card/recette-card.component';
import { RecetteModalComponent } from './recette-modal/recette-modal.component';

@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [RecetteCardComponent, RecetteModalComponent],
    exports: [RecetteCardComponent, RecetteModalComponent]
})
export class CardsModule {
}
