import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Recette } from '../../../shared/model/recette.model';

export interface RecetteDialogData {
    recette: Recette;
}

@Component({
    selector: 'app-recette-modal',
    templateUrl: './recette-modal.component.html'
})
export class RecetteModalComponent implements OnInit {

    public recette: Recette;

    constructor(
        public dialogRef: MatDialogRef<RecetteModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: RecetteDialogData) {
        this.recette = data.recette;
    }

    ngOnInit(): void {
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

}
