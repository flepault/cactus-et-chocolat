import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { CardsModule } from './cards/cards.module';
import { ArticlesComponent } from './articles/articles.component';
import { GalleryComponent } from './gallery/gallery.component';
import { InfoCardComponent } from './info-card/info-card.component';
import { NgxImageGalleryModule } from 'ngx-image-gallery';

@NgModule({
    imports: [SharedModule, CardsModule, NgxImageGalleryModule],
    declarations: [ArticlesComponent, GalleryComponent, InfoCardComponent],
    exports: [ArticlesComponent, GalleryComponent, InfoCardComponent]
})
export class LayoutModule {
}
