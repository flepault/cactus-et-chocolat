import { Component, OnInit } from '@angular/core';
import { Recette } from '../../shared/model/recette.model';
import { Store } from '@ngxs/store';
import { ReadRecettes } from '../../state/firebase/firebase.action';
import { FirebaseState } from '../../state/firebase/firebase.state';
import { filter } from 'rxjs/operators';
import { ResponsiveService } from '../../shared/service/responsive/responsive.service';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-recette',
    templateUrl: './articles.component.html',
    styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit {

    public recettes: Recette[] = [];
    public offset = 0;
    public next = false;
    public isMobile$: Observable<boolean>;

    constructor(private store: Store,
                private responsiveService: ResponsiveService) {
    }

    ngOnInit(): void {
        this.isMobile$ = this.responsiveService.isMobile();
        this.getAllRecettes();
        this.subscribeRecette();
        this.subscribeNextRecettes();
    }

    public previousPage(): void {
        this.offset--;
        this.recettes = [];
        window.scrollTo(0, 0);
        this.getAllRecettes();
    }

    public nextPage(): void {
        this.offset++;
        this.recettes = [];
        window.scrollTo(0, 0);
        this.getAllRecettes();
    }

    private getAllRecettes(): void {
        this.store.dispatch(new ReadRecettes(this.offset));
    }

    private subscribeNextRecettes(): void {
        this.store
            .select(FirebaseState.next)
            .subscribe((next: boolean) => (this.next = next));
    }

    private subscribeRecette(): void {
        this.store
            .select(FirebaseState.recettes)
            .pipe(filter(recettes => !!recettes), filter(recettes => recettes.length !== 0))
            .subscribe((recettes: Recette[]) => (this.recettes = recettes.map((recette) => {
                return ({
                    ...recette,
                    selected: false
                });
            })));
    }

}
