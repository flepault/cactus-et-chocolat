import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Actions, ofActionDispatched, Store } from '@ngxs/store';
import * as Editor from '../../../../assets/ckeditor/ckeditor';
import { CkeditorService } from '../../../shared/service/ckeditor/ckeditor.service';
import { AddRecette } from '../../../state/firebase/firebase.action';
import { AddRecetteComplete } from '../../../state/firebase/firebase.event';

@Component({
    selector: 'app-ajouter-recette-modal',
    templateUrl: './ajouter-recette-modal.component.html',
    styleUrls: ['./ajouter-recette-modal.component.css']
})
export class AjouterRecetteModalComponent implements OnInit {

    public model = {
        editorData: '<p>Hello, world!</p>'
    };

    public ajouterRecetteForm: FormGroup;
    public Editor = Editor;

    public editorConfig;

    constructor(
        public dialogRef: MatDialogRef<AjouterRecetteModalComponent>,
        private store: Store,
        private actions: Actions,
        private ckeditorService: CkeditorService
    ) {
        this.ajouterRecetteForm = new FormGroup({
            titre: new FormControl('', Validators.required),
            description: new FormControl('', Validators.required),
            texte: new FormControl(this.model.editorData, Validators.required)
        });
    }

    ngOnInit(): void {
        this.editorConfig = this.ckeditorService.generateCkEditorConfig();
        this.subscribeAddRecetteComplete();
    }

    private subscribeAddRecetteComplete(): void {
        this.actions.pipe(ofActionDispatched(AddRecetteComplete))
            .subscribe(() => {
                this.dialogRef.close();
            });
    }

    public annuler(): void {
        this.dialogRef.close();
    }

    public valider(): void {
        if (this.ajouterRecetteForm.valid) {
            this.store.dispatch(
                new AddRecette(
                    {
                        titre: this.ajouterRecetteForm.controls.titre.value,
                        description: this.ajouterRecetteForm.controls.description.value,
                        texte: this.model.editorData
                    },
                )
            );
        }
    }
}
