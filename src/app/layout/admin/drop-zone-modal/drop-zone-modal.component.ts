import { HttpErrorResponse, HttpEvent, HttpEventType } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngxs/store';
import { of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { UploadFileResponseDTO } from '../../../shared/model/upload.model';
import { StorageService } from '../../../shared/service/storage/storage.service';
import { UpdateGallery } from '../../../state/firebase/firebase.action';

export interface FileUpload {
    file: File;
    inProgress: boolean;
    progress: number;
}

@Component({
    selector: 'app-drop-zone-modal',
    templateUrl: './drop-zone-modal.component.html'
})
export class DropZoneModalComponent implements OnInit {

    @ViewChild('fileUpload', {static: false})
    fileUpload: ElementRef;
    files = [];

    constructor(private storageService: StorageService, private store: Store) {
    }

    ngOnInit() {
    }

    private uploadFile(fileUpload: FileUpload): void {

        const image: FormData = new FormData();
        image.append(
            fileUpload.file.name,
            fileUpload.file,
            fileUpload.file.name
        );

        fileUpload.inProgress = true;

        this.storageService.uploadImageGalerie(image).pipe(
            map((event: HttpEvent<UploadFileResponseDTO>) => {
                switch (event.type) {
                    case HttpEventType.UploadProgress:
                        fileUpload.progress = Math.round(event.loaded * 100 / event.total);
                        break;
                    case HttpEventType.Response:
                        return event;
                }
            }),
            catchError((error: HttpErrorResponse) => {
                fileUpload.inProgress = false;
                return of(`${fileUpload.file.name} upload failed: ${error}`);
            })).subscribe((event: any) => {
            if (typeof (event) === 'object') {
                return this.store.dispatch(new UpdateGallery(event.body));
            }
        });
    }

    private uploadFiles() {
        this.fileUpload.nativeElement.value = '';
        this.files.forEach(file => {
            this.uploadFile(file);
        });
    }

    public onClick(): void {
        const fileUpload = this.fileUpload.nativeElement;
        fileUpload.onchange = () => {
            for (let i = 0, len = fileUpload.files.length; i < len; i++) {
                this.files.push({file: fileUpload.files[i], inProgress: false, progress: 0});
            }
            this.uploadFiles();
        };
        fileUpload.click();
    }

}
