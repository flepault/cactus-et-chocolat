import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { Actions, ofActionDispatched, Store } from '@ngxs/store';
import { AuthentificationService } from '../../../shared/service/authentification/authentification.service';
import { LoginComplete } from '../../../state/firebase/firebase.event';
import { AjouterRecetteModalComponent } from '../ajouter-recette-modal/ajouter-recette-modal.component';
import { DropZoneModalComponent } from '../drop-zone-modal/drop-zone-modal.component';
import { LoginModalComponent } from '../login-modal/login-modal.component';

@Component({
    selector: 'app-admin-action',
    templateUrl: './admin-action.component.html'
})
export class AdminActionComponent implements OnInit {

    public logged = false;

    constructor(
        public dialog: MatDialog,
        public translate: TranslateService,
        private actions: Actions,
        private store: Store,
        private authentificationService: AuthentificationService) {
        translate.addLangs(['en', 'fr']);
        translate.setDefaultLang('fr');
    }

    ngOnInit(): void {
        this.logged = this.authentificationService.isLoggedIn();
        this.subscribeLoginComplete();
    }

    private subscribeLoginComplete(): void {
        this.actions.pipe(ofActionDispatched(LoginComplete))
            .subscribe(() => {
                this.logged = true;
            });
    }

    public logout(): void {
        this.authentificationService.logout();
        this.logged = false;
    }

    public openDialogDropZone(): void {
        const dialogRef = this.dialog.open(DropZoneModalComponent);

        dialogRef.afterClosed().subscribe(result => {
        });
    }

    public openDialogAjouterRecette(): void {
        const dialogRef = this.dialog.open(AjouterRecetteModalComponent, {disableClose: true});

        dialogRef.afterClosed().subscribe(result => {
        });
    }

    public openDialogLogin(): void {
        const dialogRef = this.dialog.open(LoginModalComponent);

        dialogRef.afterClosed().subscribe(result => {
        });
    }

}
