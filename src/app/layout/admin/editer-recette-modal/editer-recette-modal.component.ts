import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Actions, ofActionDispatched, Store } from '@ngxs/store';
import * as Editor from '../../../../assets/ckeditor/ckeditor';
import { CkeditorService } from '../../../shared/service/ckeditor/ckeditor.service';
import { UpdateRecette } from '../../../state/firebase/firebase.action';
import { UpdateRecetteComplete } from '../../../state/firebase/firebase.event';
import { RecetteDialogData } from '../../cards/recette-modal/recette-modal.component';

@Component({
    selector: 'app-editer-recette-modal',
    templateUrl: './editer-recette-modal.component.html',
    styleUrls: ['./editer-recette-modal.component.css']
})
export class EditerRecetteModalComponent implements OnInit {

    public model = {
        editorData: ''
    };

    public editerRecetteForm: FormGroup;
    public Editor = Editor;

    public editorConfig;

    constructor(
        public dialogRef: MatDialogRef<EditerRecetteModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: RecetteDialogData,
        private store: Store,
        private actions: Actions,
        private ckeditorService: CkeditorService
    ) {

        this.model.editorData = data.recette.texte;
        this.editerRecetteForm = new FormGroup({
            titre: new FormControl(data.recette.titre, Validators.required),
            description: new FormControl(data.recette.description, Validators.required),
            texte: new FormControl(this.model.editorData, Validators.required)
        });
    }

    ngOnInit(): void {
        this.editorConfig = this.ckeditorService.generateCkEditorConfig();
        this.subscribeEditerRecetteComplete();
    }

    private subscribeEditerRecetteComplete(): void {
        this.actions.pipe(ofActionDispatched(UpdateRecetteComplete))
            .subscribe(() => {
                this.dialogRef.close();
            });
    }

    public annuler(): void {
        this.dialogRef.close();
    }

    public valider(): void {
        if (this.editerRecetteForm.valid) {
            this.store.dispatch(
                new UpdateRecette(
                    {
                        id: this.data.recette.id,
                        dateCreation: this.data.recette.dateCreation,
                        titre: this.editerRecetteForm.controls.titre.value,
                        description: this.editerRecetteForm.controls.description.value,
                        texte: this.model.editorData
                    },
                )
            );
        }
    }
}
