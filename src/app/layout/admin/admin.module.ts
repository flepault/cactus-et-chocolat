import { NgModule } from '@angular/core';

import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { StorageService } from '../../shared/service/storage/storage.service';
import { SharedModule } from '../../shared/shared.module';
import { AdminActionComponent } from './admin-action/admin-action.component';
import { AjouterRecetteModalComponent } from './ajouter-recette-modal/ajouter-recette-modal.component';
import { DropZoneModalComponent } from './drop-zone-modal/drop-zone-modal.component';
import { EditerRecetteModalComponent } from './editer-recette-modal/editer-recette-modal.component';
import { LoginModalComponent } from './login-modal/login-modal.component';

@NgModule({
    declarations: [
        AdminActionComponent,
        AjouterRecetteModalComponent,
        EditerRecetteModalComponent,
        DropZoneModalComponent,
        LoginModalComponent
    ],
    exports: [
        AdminActionComponent,
        AjouterRecetteModalComponent,
        EditerRecetteModalComponent,
        DropZoneModalComponent
    ],
    imports: [SharedModule, CKEditorModule],
    providers: [StorageService]
})
export class AdminModule {
}
