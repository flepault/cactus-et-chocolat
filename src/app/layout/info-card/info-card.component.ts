import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ResponsiveService } from '../../shared/service/responsive/responsive.service';

@Component({
    selector: 'app-info-card',
    templateUrl: './info-card.component.html',
    styleUrls: ['./info-card.component.css']
})
export class InfoCardComponent implements OnInit {

    public isMobile$: Observable<boolean>;

    constructor(private responsiveService: ResponsiveService) {
    }

    ngOnInit() {
        this.isMobile$ = this.responsiveService.isMobile();
    }

}
