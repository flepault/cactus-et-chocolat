import { TestBed } from '@angular/core/testing';
import { SharedModule } from '../../shared/shared.module';
import { InfoCardComponent } from './info-card.component';


describe('InfoCardComponent', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [SharedModule],
            declarations: [InfoCardComponent]
        }).compileComponents();
    });

    it('should create the app', () => {
        const fixture = TestBed.createComponent(InfoCardComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });
});
