import { Component, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngxs/store';
import { GALLERY_CONF, NgxImageGalleryComponent } from 'ngx-image-gallery';
import { Image } from '../../shared/model/image.model';
import { FirebaseState } from '../../state/firebase/firebase.state';
import { ResponsiveService } from '../../shared/service/responsive/responsive.service';
import { Observable } from 'rxjs';

export interface ImageDialogData {
    images: Image[];
}

@Component({
    selector: 'app-gallery',
    templateUrl: './gallery.component.html',
    styleUrls: ['gallery.component.css'],
})
export class GalleryComponent implements OnInit {

    @ViewChild(NgxImageGalleryComponent) ngxImageGallery: NgxImageGalleryComponent;

    confLaptop: GALLERY_CONF = {
        imageOffset: '0px',
        showDeleteControl: false,
        showImageTitle: false,
        inline: true,
        backdropColor: 'white',
        reactToKeyboard: true,
        showCloseControl: false,
        showArrows: true
    };

    confMobile: GALLERY_CONF = {
        imageOffset: '0px',
        showDeleteControl: false,
        showImageTitle: false,
        inline: true,
        backdropColor: 'white',
        reactToKeyboard: true,
        showCloseControl: false,
        showArrows: false
    };

    conf = this.confLaptop;

    images: Image[] = [];

    constructor(public store: Store, public responsiveService: ResponsiveService) {
    }

    ngOnInit(): void {
        this.subscribeResponsive();
        this.subsribeGallery();
    }

    private subscribeResponsive() {
        this.responsiveService.isMobile().subscribe((isMobile: boolean) =>
            isMobile ? this.conf = this.confMobile : this.conf = this.confLaptop)
    }

    private subsribeGallery(): void {
        this.store
            .select(FirebaseState.images)
            .subscribe((images: Image[]) => (this.images = images.map((image) => ({
                ...image,
                selected: false
            }))));
    }

    openGallery(index: number = 0): void {
        this.ngxImageGallery.open(index);
    }

    // close gallery
    closeGallery() {
        this.ngxImageGallery.close();
    }

    // set new active(visible) image in gallery
    newImage(index: number = 0) {
        this.ngxImageGallery.setActiveImage(index);
    }

    // next image in gallery
    nextImage(index: number = 0) {
        this.ngxImageGallery.next();
    }

    // prev image in gallery
    prevImage(index: number = 0) {
        this.ngxImageGallery.prev();
    }

    /**************************************************/

    // EVENTS
    // callback on gallery opened
    galleryOpened(index) {
    }

    // callback on gallery closed
    galleryClosed() {
    }

    // callback on gallery image clicked
    galleryImageClicked(index) {
        this.newImage(index+1);
    }

    // callback on gallery image changed
    galleryImageChanged(index) {
    }

    // callback on user clicked delete button
    deleteImage(index) {
    }
}
