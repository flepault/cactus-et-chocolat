import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArticlesComponent } from './layout/articles/articles.component';
import { GalleryComponent } from './layout/gallery/gallery.component';
import { InfoCardComponent } from './layout/info-card/info-card.component';
import { RecetteCardComponent } from './layout/cards/recette-card/recette-card.component';


const article = 'article/';

const homeRoute = ''
const articlesRoute = 'articles';
const galleryRoute = 'gallery';
const infosRoute = 'infos';
const articleRoute = article+':id';

const routes: Routes = [
    {path: homeRoute, component: ArticlesComponent},
    {path: articlesRoute, component: ArticlesComponent},
    {path: galleryRoute, component: GalleryComponent},
    {path: infosRoute, component: InfoCardComponent},
    {path: articleRoute, component: RecetteCardComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
