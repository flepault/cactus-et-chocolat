import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { patch, updateItem } from '@ngxs/store/operators';
import { first } from 'rxjs/operators';
import { Image } from '../../shared/model/image.model';
import { ReadRecettesModel, Recette } from '../../shared/model/recette.model';
import { AuthentificationService } from '../../shared/service/authentification/authentification.service';
import { RecetteService } from '../../shared/service/recette/recette.service';
import { StorageService } from '../../shared/service/storage/storage.service';
import { AddRecette, LoadGallery, Login, ReadRecettes, UpdateGallery, UpdateRecette } from './firebase.action';
import {
    AddRecetteComplete,
    AddRecetteError,
    LoadGalleryComplete,
    LoadGalleryError,
    LoginComplete,
    LoginError,
    ReadRecettesComplete,
    ReadRecettesError,
    UpdateRecetteComplete,
    UpdateRecetteError
} from './firebase.event';


export interface FirebaseStateModel {
    recettes: Array<Recette>;
    next: boolean;
    images: Array<Image>;
}

@State<FirebaseStateModel>({
    name: 'FirebaseState',
    defaults: {
        recettes: [],
        next: false,
        images: [],
    }
})
@Injectable()
export class FirebaseState {
    constructor(
        private recetteService: RecetteService,
        private storageService: StorageService,
        private authentificationService: AuthentificationService
    ) {
    }

    @Selector()
    public static recettes(state: FirebaseStateModel): Recette[] {
        return state.recettes.slice(0, 6);
    }

    @Selector()
    public static next(state: FirebaseStateModel): boolean {
        return state.next;
    }

    @Selector()
    public static images(state: FirebaseStateModel): Image[] {
        return state.images;
    }

    @Action(AddRecette)
    public addRecette(ctx: StateContext<FirebaseStateModel>, action: AddRecette) {
        const state = ctx.getState();

        this.recetteService.addRecette(action.recette)
            .pipe(first())
            .subscribe(
                (recette: Recette) => {
                    ctx.patchState({
                        recettes: [recette, ...state.recettes]
                    });
                    ctx.dispatch(new AddRecetteComplete(recette));
                },
                err => ctx.dispatch(new AddRecetteError(err))
            );
    }

    @Action(UpdateRecette)
    public updateRecette(ctx: StateContext<FirebaseStateModel>, action: UpdateRecette) {
        const state = ctx.getState();

        this.recetteService.updateRecette(action.recette)
            .pipe(first())
            .subscribe(
                (recette: Recette) => {
                    ctx.setState(
                        patch({
                            recettes: updateItem<Recette>(recetteState => recette.id === recetteState.id, recette)
                        }));
                    ctx.dispatch(new UpdateRecetteComplete(recette));
                },
                err => ctx.dispatch(new UpdateRecetteError(err))
            );
    }

    @Action(ReadRecettes)
    public readRecettes(
        ctx: StateContext<FirebaseStateModel>,
        action: ReadRecettes
    ) {
        this.recetteService
            .getRecettes(action.offset.toString())
            .pipe(first())
            .subscribe((readRecettes: ReadRecettesModel) => {
                ctx.patchState(
                    {
                        recettes: readRecettes.recettes.map((recette) => ({
                            ...recette,
                            selected: false
                        })),
                        next: readRecettes.next
                    });
                ctx.dispatch(new ReadRecettesComplete());
            }, err => ctx.dispatch(new ReadRecettesError(err)));
    }

    @Action(LoadGallery)
    public loadGallery(
        ctx: StateContext<FirebaseStateModel>
    ) {
        this.storageService
            .getAllImageGalerie()
            .pipe(first())
            .subscribe((images: Image[]) => {
                ctx.patchState({
                    images: images.map((image) => ({
                        ...image,
                        selected: false
                    }))
                });
                ctx.dispatch(new LoadGalleryComplete());
            }, err => ctx.dispatch(new LoadGalleryError(err)));
    }

    @Action(UpdateGallery)
    public updateGallery(ctx: StateContext<FirebaseStateModel>, action: UpdateGallery) {
        const state = ctx.getState();
        ctx.patchState({
            images: [action.image, ...state.images]
        });
    }

    @Action(Login)
    public login(ctx: StateContext<FirebaseStateModel>, action: Login) {
        this.authentificationService.login({email: action.email, password: action.password, returnSecureToken: true})
            .pipe(first())
            .subscribe(() => {
                ctx.dispatch(new LoginComplete());
            }, err => ctx.dispatch(new LoginError(err.error.error.message)));
    }
}
