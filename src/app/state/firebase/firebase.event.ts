import { Recette } from '../../shared/model/recette.model';

export class AddRecetteComplete {
    static readonly type = 'Firebase.addRecetteComplete';

    constructor(public recette: Recette) {
    }
}

export class AddRecetteError {
    static readonly type = 'Firebase.addRecetteError';

    constructor(public err: string) {
    }
}

export class UpdateRecetteComplete {
    static readonly type = 'Firebase.updateRecetteComplete';

    constructor(public recette: Recette) {
    }
}

export class UpdateRecetteError {
    static readonly type = 'Firebase.updateRecetteError';

    constructor(public err: string) {
    }
}

export class ReadRecettesComplete {
    static readonly type = 'Firebase.readRecetteComplete';

    constructor() {
    }
}

export class ReadRecettesError {
    static readonly type = 'Firebase.readRecetteError';

    constructor(public err: string) {
    }
}

export class LoadGalleryComplete {
    static readonly type = 'Firebase.loadGalleryComplete';

    constructor() {
    }
}

export class LoadGalleryError {
    static readonly type = 'Firebase.loadGalleryError';

    constructor(public err: string) {
    }
}

export class LoginComplete {
    static readonly type = 'Firebase.loginComplete';

    constructor() {
    }
}

export class LoginError {
    static readonly type = 'Firebase.loginError';

    constructor(public err: string) {
    }
}
