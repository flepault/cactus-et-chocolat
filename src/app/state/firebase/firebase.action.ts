import { Image } from 'src/app/shared/model/image.model';
import { Recette } from '../../shared/model/recette.model';

export class AddRecette {
    static readonly type = 'Firebase.addRecette';

    constructor(public recette: Recette) {
    }
}

export class UpdateRecette {
    static readonly type = 'Firebase.updateRecette';

    constructor(public recette: Recette) {
    }
}

export class ReadRecettes {
    static readonly type = 'Firebase.readRecette';

    constructor(public offset: number) {
    }
}

export class LoadGallery {
    static readonly type = 'Firebase.loadGallery';

    constructor() {
    }
}

export class UpdateGallery {
    static readonly type = 'Firebase.updateGallery';

    constructor(public image: Image) {
    }
}

export class Login {
    static readonly type = 'Firebase.login';

    constructor(public email: string, public password: string) {
    }
}

