export const fontFamilyConfig = {
    options: [
        'default',
        'Arial, Helvetica, sans-serif',
        'Courier New, Courier, monospace',
        'Georgia, serif',
        'Lucida Sans Unicode, Lucida Grande, sans-serif',
        'Tahoma, Geneva, sans-serif',
        'Times New Roman, Times, serif',
        'Trebuchet MS, Helvetica, sans-serif',
        'Verdana, Geneva, sans-serif'
    ]
};

export const imageConfig = {
    toolbar: [
        'imageTextAlternative',
        '|',
        'imageStyle:full',
        'imageStyle:alignLeft',
        'imageStyle:alignCenter',
        'imageStyle:alignRight'
    ],
    styles: ['full', 'alignLeft', 'alignCenter', 'alignRight']
};
