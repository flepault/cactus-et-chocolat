export interface LoginRequest {
    email: string;
    password: string;
    returnSecureToken: boolean;
}

export interface LoginResponse {
    idToken: string;
    email: string;
    refreshToken: string;
    expiresIn: number;
    localId: string;
}
