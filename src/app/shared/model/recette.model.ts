export interface Recette {
    id?: string;
    dateCreation?: DateRecette;
    titre: string;
    description: string;
    texte: string;
    image?: string;
}

export interface DateRecette {
    _seconds: number;
    _nanoseconds: number;
}

export interface ReadRecettesModel {
    recettes: Recette[];
    next: boolean;
}

export interface ReadRecetteModel {
    recette: Recette;
}
