import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {
    public intercept(
        req: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        if (req.method === 'POST' || req.method === 'PUT') {
            const idToken = sessionStorage.getItem('id_token');

            if (!!idToken) {
                const cloned = req.clone({
                    headers: req.headers.set('Authorization', idToken)
                });

                return next.handle(cloned);
            } else {
                return next.handle(req);
            }
        } else {
            return next.handle(req);
        }
    }
}
