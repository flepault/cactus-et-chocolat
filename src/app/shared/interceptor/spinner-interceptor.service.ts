import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable } from 'rxjs';
import { delay, finalize } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class SpinnerInterceptor implements HttpInterceptor {

    private counter = 0;

    constructor(private spinnerService: NgxSpinnerService) {
    }

    public intercept(
        req: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {

        if (req.method === 'POST' || req.method === 'PUT') {
            if (this.counter === 0) {
                this.spinnerService.show();
            }

            this.counter++;

            return next.handle(req).pipe(
                delay(100),
                finalize(() => {
                    this.counter--;
                    if (this.counter === 0) {
                        this.spinnerService.hide();
                    }
                })
            );
        } else {
            return next.handle(req);
        }
    }
}
