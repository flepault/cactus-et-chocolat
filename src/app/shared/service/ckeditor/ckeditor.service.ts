import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { fontFamilyConfig, imageConfig } from '../../../shared/config/ckeditor-config';

@Injectable({
    providedIn: 'root'
})
export class CkeditorService {

    constructor() {
    }

    public generateCkEditorConfig() {
        return {
            fontFamily: fontFamilyConfig,
            simpleUpload: {
                uploadUrl: environment.firebaseUploadImageURL,
                headers: {
                    Authorization: sessionStorage.getItem('id_token')
                }
            },
            image: imageConfig
        };
    }
}
