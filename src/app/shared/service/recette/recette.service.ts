import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { ReadRecetteModel, ReadRecettesModel, Recette } from '../../../shared/model/recette.model';


@Injectable({
    providedIn: 'root'
})
export class RecetteService {

    constructor(private http: HttpClient) {
    }

    // GET
    public getRecette(id: string): Observable<ReadRecetteModel> {
        return this.http.get<ReadRecetteModel>(environment.firebaseHttpURL + '/getRecette', {
            params: {
                id
            }
        });
    }

    // GET ALL
    public getRecettes(offset: string): Observable<ReadRecettesModel> {
        return this.http.get<ReadRecettesModel>(environment.firebaseHttpURL + '/getRecettes', {
            params: {
                offset
            }
        });
    }

    // ADD
    public addRecette(recette: Recette): Observable<Recette> {
        return this.http.post<Recette>(environment.firebaseHttpURL + '/addRecette', recette);
    }

    // UPDATE
    public updateRecette(recette: Recette): Observable<Recette> {
        return this.http.put<Recette>(environment.firebaseHttpURL + '/updateRecette/' + recette.id, recette);
    }

}
