import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';

import { NgxsModule } from '@ngxs/store';
import { environment } from '../environments/environment';
import { AppComponent } from './app.component';
import { AdminModule } from './layout/admin/admin.module';
import { LayoutModule } from './layout/layout.module';

import { SharedModule } from './shared/shared.module';
import { FirebaseState } from './state/firebase/firebase.state';
import { StateModule } from './state/state.module';
import { AppRoutingModule } from './app.routing.module';

@NgModule({
    imports: [
        AppRoutingModule,
        LayoutModule,
        SharedModule,
        AdminModule,
        StateModule,
        [NgxsModule.forRoot([FirebaseState], {developmentMode: !environment.production})],
        NgxsReduxDevtoolsPluginModule.forRoot({disabled: environment.production})
    ],
    declarations: [AppComponent],
    bootstrap: [AppComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
}
